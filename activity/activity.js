-What directive is used by Node.js in loading the modules it needs? 
import directive

- What Node.js module contains a method for server creation?
http module

- What is the method of the http object responsible for creating a server using Node.js?
	createServer() method
- What method of the response object allows us to set status codes and content types?
		res()
- Where will console.log() output its contents when run in Node.js?
		browser
- What property of the request object contains the address's endpoint?
		req.url