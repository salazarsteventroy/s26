const http = require ('http');
const port = 3000

const server = http.createServer((req, res) => {
	// http://localhost:3000/login
	if (req.url == '/login'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('you are in the login page')
	}
	else {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('I am sorry the page that you are looking for cannot be found')
	}

})



server.listen(port);


console.log(`Server now accessible at localhost:${port}`)
