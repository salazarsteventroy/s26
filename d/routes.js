const http = require ('http');

// Create a variable "port to store the port number"

const port = 4000

// create a variable 'server that stores that output of the create server'

const server = http.createServer((req, res) => {
	// http://localhost:4000/greeting
	if (req.url == '/greeting'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Hello Again')
	}
	else if (req.url == '/homepage'){
	// http://localhost:4000/homepage
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('This is the homepage')
	}
	else {
	res.writeHead(200, {'Content-Type': 'text/plain'})
	res.end('Page not found Status 404')
}

})





// use the 'server and 'port' variables created above

server.listen(port);

// when server is running , console will print the message:

console.log(`Server now accessible at localhost:${port}`)




/* npm install -g nodemon
$ nodemon routes.js

*/